package cn.chowa.ejyy.common;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class CodeException extends RuntimeException {

    private int code;

    public CodeException(int code, String msg) {
        super(msg);
        this.code = code;
    }

}
