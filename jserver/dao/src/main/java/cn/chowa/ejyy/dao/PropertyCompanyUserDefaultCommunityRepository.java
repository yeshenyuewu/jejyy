package cn.chowa.ejyy.dao;

import cn.chowa.ejyy.model.entity.PropertyCompanyUserDefaultCommunity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PropertyCompanyUserDefaultCommunityRepository extends JpaRepository<PropertyCompanyUserDefaultCommunity, Long> {

    PropertyCompanyUserDefaultCommunity findByPropertyCompanyUserId(long propertyCompanyUserId);

}
